package com.saponchet.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var button1 = findViewById<Button>(R.id.button2)
        button1.setOnClickListener {
            val intent = Intent (this,HelloActivity::class.java)
            intent.putExtra("stu_name","Saponchet Warakijsathapohn")
            startActivity(intent)

            val nameTextView: TextView = findViewById(R.id.stu_name)
            val idTextView: TextView = findViewById(R.id.stu_id)
            Log.d(TAG,""+nameTextView.text)
            Log.d(TAG,""+idTextView.text)
        }

    }
//    override fun onStart() {
//        super.onStart()
//        Log.d(TAG, "onStart Called")
//    }
//
//    override fun onResume() {
//        super.onResume()
//        Log.d(TAG, "onResume Called")
//    }
//
//    override fun onPause() {
//        super.onPause()
//        Log.d(TAG, "onPause Called")
//    }
//
//    override fun onStop() {
//        super.onStop()
//        Log.d(TAG, "onStop Called")
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        Log.d(TAG, "onDestroy Called")
//    }
//
//    override fun onRestart() {
//        super.onRestart()
//        Log.d(TAG, "onRestart Called")
//    }

}